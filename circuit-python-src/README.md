# CircuitPython documentation

The library files in the 'lib' directory are originally from Adafruit's bundle of handy CircuitPython code. The version that ships with the Tiny TRS-80s was released on 5 September 2023. You can find that bundle on the [release page for that day](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/tag/20230905) with the file name 'adafruit-circuitpython-bundle-8.x-mpy-20230905.zip'. [direct link](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/download/20230905/adafruit-circuitpython-bundle-8.x-mpy-20230905.zip)

To add a library, unzip that bundle and find the referenced library (e.g. `adafruit_ble_radio.mpy`) and copy it into the `lib` directory on your tiny model III. Your code.py file will then be able to import that library like `import adafruit_ble_radio`..

To set up WiFi, copy settings.toml.example to settings.toml (in the same directory) and then edit it to enter your WiFi SSID and password.
