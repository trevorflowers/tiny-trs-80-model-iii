import os
import ssl
import rtc
import wifi
import time
import board
import pwmio
import ipaddress
import displayio
import terminalio
import socketpool

import adafruit_ntp
from adafruit_st7789 import ST7789
from adafruit_display_text import label

print("Starting TRS-80 Clock")

timezone_offset = -7
font_color = 0x55AA55
font_scale = 9
line_height = 30
background_color = 0x111111
left_margin = 15

# Set up the display
displayio.release_displays()
spi = board.SPI()
display_bus = displayio.FourWire(
    spi,
    command=board.D16,
    chip_select=board.D5,
    reset=board.D9
)
display = ST7789(display_bus, width=280, height=240, rowstart=20, rotation=270)

# Set up drawing utils
splash = displayio.Group()
display.show(splash)
color_bitmap = displayio.Bitmap(280, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = background_color
bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)

# Create a clock
real_time_clock = rtc.RTC()

# Try to set up the network
pool = None
try:
    wifi.radio.connect(os.getenv('CIRCUITPY_WIFI_SSID'), os.getenv('CIRCUITPY_WIFI_PASSWORD')) # Depends on settings.toml for Wifi config
    pool = socketpool.SocketPool(wifi.radio)
    print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])
    print("My IP address is", wifi.radio.ipv4_address)
except Exception as error:
    print("Error setting up the network", error)

def fetch_time():
    print("Fetching time")
    if not pool:
        return False
    try:
        real_time_clock.datetime = adafruit_ntp.NTP(pool, tz_offset=timezone_offset).datetime
        print("Fetched time")
        return True
    except Exception as error:
        print("Exception fetching time", error)
        return False

def draw_label(txt, scale, color, x, y):
    text_group = displayio.Group(scale=scale, x=x, y=y)
    text_area = label.Label(terminalio.FONT, text=txt, color=color)
    text_group.append(text_area)  # Subgroup for text scaling
    splash.append(text_group)

def draw_time(dt):
    print("Drawing time")
    time_string = "{0:02}:{1:02}".format(dt.tm_hour, dt.tm_min)
    draw_label(time_string, font_scale, font_color, 10, 80)

print("Starting the loop")
last_hour = -1
last_minute = -1
has_fetched_time = False
while True:
    should_fetch = has_fetched_time is False
    should_draw = False
    if real_time_clock.datetime.tm_hour != last_hour:
        should_fetch = True # Check the network time once an hour
        should_draw = True
        last_hour = real_time_clock.datetime.tm_hour
    if real_time_clock.datetime.tm_min != last_minute:
        should_draw = True
        last_minute = real_time_clock.datetime.tm_min
    if should_fetch:
        has_fetched_time = has_fetched_time or fetch_time()
    if should_draw:
        splash.pop()
        draw_time(real_time_clock.datetime)
    time.sleep(5)


