# Tiny TRS-80 Model III docs and examples

This repository contains the documentation and example code for the Electric Tiny TRS-80 Model III from [Trevor Flowers at Transmutable.com](https://transmutable.com/).

That's a mouthful so for the rest of this document we'll call it the *eM3*. 

This document also could be useful for anyone using an [AdaFruit QT Py / XIAO](https://www.adafruit.com/category/595) and one of the [EyeSPY displays](https://www.adafruit.com/category/63).

The eM3 comes with these specific boards:
- [QT Py ESP32-S3](https://www.adafruit.com/product/5426)
- [EyeSPI BFF](https://www.adafruit.com/product/5772)
- [1.69" Round Rectangle IPS display](https://www.adafruit.com/product/5206) 

Here are the excellent Adafruit learning guides for these boards:
- [QT Py](https://learn.adafruit.com/adafruit-qt-py-esp32-s3)
- [BFF](https://learn.adafruit.com/adafruit-eyespi-bff) 
- [display guide](https://learn.adafruit.com/adafruit-1-69-280x240-round-rectangle-color-ips-tft-display)

Hot tip: Open up those guides when you're programming as they demonstrate how to use All The Things in both CircuitPython and Arduino.


